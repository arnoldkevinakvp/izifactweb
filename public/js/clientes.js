document.addEventListener("DOMContentLoaded", function() {
  const slide = document.querySelector('.carousel-slide');
  const images = document.querySelectorAll('.carousel-slide img');
  let counter = 1;
  let size = images[0].clientWidth;

  // Ajustar el tamaño de las imágenes y la posición inicial del carrusel
  function setupCarousel() {
    size = slide.offsetWidth;
    slide.style.transform = 'translateX(' + (-size * counter) + 'px)';
  }

  window.addEventListener('resize', setupCarousel);
  setupCarousel(); // Llamar a la función en la carga inicial

  // Función para moverse al siguiente logotipo
  function moveToNextLogo() {
    if (counter >= images.length - 1) return;
    slide.style.transition = "transform 0.5s ease-in-out";
    counter++;
    slide.style.transform = 'translateX(' + (-size * counter) + 'px)';
  }

  // Establecer el intervalo para moverse al siguiente logotipo cada 3 segundos
  let autoSlideInterval = setInterval(moveToNextLogo, 3000);

  // Agregar un evento 'transitionend' para "reiniciar" el carrusel si es necesario
  slide.addEventListener('transitionend', () => {
    if (images[counter].id === 'lastClone') {
      slide.style.transition = "none"; // No mostrar la transición al "reiniciar" el carrusel
      counter = images.length - 2;
      slide.style.transform = 'translateX(' + (-size * counter) + 'px)';
    }
    if (images[counter].id === 'firstClone') {
      slide.style.transition = "none";
      counter = 1;
      slide.style.transform = 'translateX(' + (-size * counter) + 'px)';
    }
  });
});
