<?php

namespace App\Http\Collection;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ContactCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return $this->collection->transform(function($row, $key) {
            return [
                'id' => $row->id,
                'name' => $row->name,
                'RUC' => $row->RUC,
                'phone' => $row->phone,
                'plan' => $row->plan,
                'mounth' => $row->mounth ? (($row->mounth == '01') ? 'Mensual' : (($row->mounth == '03') ? 'Trimestral' : (($row->mounth == '06') ? 'Semestral' : (($row->mounth == '12') ? 'Anual' : (($row->mounth == '24') ? '2 Años' : '3 Años'))))) : '',
                'state' => $row->state,
                'created_at' => $row->created_at ? $row->created_at->format('d/m/Y') : null,
                'updated_at' => $row->updated_at ? $row->updated_at->format('d/m/Y') : null,
            ];
        });
    }

}
