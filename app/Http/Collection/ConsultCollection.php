<?php

namespace App\Http\Collection;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ConsultCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return $this->collection->transform(function($row, $key) {
            return [
                'id' => $row->id,
                'name' => $row->name,
                'email' => $row->email,
                'phone' => $row->phone,
                'coment' => $row->coment,
                'state' => $row->state,
                'created_at' => $row->created_at ? $row->created_at->format('d/m/Y') : null,
            ];
        });
    }

}
