<?php
namespace App\Http\Controllers;
use App\Models\UserChatbot;
use App\Models\Question;
use App\Models\Response;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class UserChatbotController extends Controller
{
    public function processGPTMessage(Request $request)
    {
        $userId = $request->input('id');
        $message = $request->input('userInput');

        // Guardar la pregunta en la base de datos
        $question = Question::create([
            'user_chatbot_id' => $userId,
            'question_text' => $message
        ]);

        // Obtener respuesta de GPT
        $client = new Client();
        $response = $client->post('https://api.openai.com/v1/chat/completions', [
            'headers' => [
                'Authorization' => 'Bearer ' . env('OPENAI_API_KEY'),
                'Content-Type' => 'application/json',
            ],
            'json' => [
                'model' => 'gpt-4o-mini',
                'messages' => [
                    ['role' => 'system', 'content' => 'Eres un asistente útil.'],
                    ['role' => 'user', 'content' => $message],
                ],
                'max_tokens' => 150,
                'temperature' => 0.7,
            ],
        ]);
        $result = json_decode($response->getBody(), true);
        $gptResponse = $result['choices'][0]['message']['content'];

        // Guardar la respuesta en la base de datos
        Response::create([
            'question_id' => $question->id,
            'response_text' => $gptResponse
        ]);

        return response()->json(['message' => $gptResponse]);
    }
    // Registrar el usuario
    public function registerUser(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:100',
            'phone' => 'required|string|max:15',
        ]);
        $user = new UserChatbot;
        $user->fill($request->all());
        $user->save();
        // return [
        //     'success' => true,
        //     'message' => "Gracias por escribirnos, en breve un asesor te contactará."
        // ];

        return response()->json(['user_chatbot_id' => $user->id, 'message' => "¡Bienvenido {$user->name}!", 'status' => 'success']);
    }

    // Procesar un mensaje del usuario
    public function processMessage(Request $request)
    {
        $request->validate([
            'id' => 'required|integer',
            'userInput' => 'required|string',
        ]);

        // Guardar la pregunta en la base de datos
        $question = Question::create([
            'user_chatbot_id' => $request->input('id'),
            'question_text' => $request->input('userInput'),
        ]);

        // Simulación de respuesta
        $botResponse = "Lo siento, no entiendo eso todavía. 🤖";

        return response()->json(['response' => $botResponse]);
    }
}
