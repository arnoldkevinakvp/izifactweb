<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function registrerLogin()
    {
        return view('auth.register');
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return response()->json(['message' => 'Login successful'], 200);
        }
    
        return response()->json(['message' => 'Las credenciales no coinciden con nuestros registros.'], 401);
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return response()->json(['message' => 'Logout successful'], 200);
    }

    public function user()
    {
        return response()->json(Auth::user());
    }
    public function registrar(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'ruc' => 'nullable|string|max:11',
            'celular' => 'nullable|string|max:15',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'ruc' => $request->ruc,
            'celular' => $request->celular,
        ]);

        Auth::login($user);

        return response()->json(['message' => 'Registro exitoso']);
    }
    // Redirige al proveedor
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    // Maneja la respuesta del proveedor
    public function handleProviderCallback($provider)
    {
        $socialUser = Socialite::driver($provider)->user();

        $user = User::updateOrCreate(
            ['email' => $socialUser->getEmail()],
            [
                'name' => $socialUser->getName(),
                'email' => $socialUser->getEmail(),
                'password' => bcrypt(str_random(24)), // Contraseña aleatoria para usuarios sociales
            ]
        );

        Auth::login($user, true);

        return redirect('/inicio'); // Redirige después del login
    }
}
