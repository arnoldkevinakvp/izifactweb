<?php

namespace App\Http\Controllers;

use App\Models\Consult;
use Illuminate\Http\Request;
use App\Http\Collection\ConsultCollection;

class ConsultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contact = new Consult;
        $contact->fill($request->all());
        $contact->save();
        return [
            'success' => true,
            'message' => "Gracias por escribirnos, en breve un asesor te contactará."
        ];
    }

    public function consultstate($id){
        $consult = Consult::find($id);
        if($consult->state == '0'){
            $consult->state = 1;
        }
        else{
            $consult->state = 0;
        }
        $consult->save();
        return [
            'success' => true,
            'message' => 'Estado cambiado con éxito'
        ];
    }

    public function consultRecords(Request $request){
        $query = $this->getRecords($request); // Obtén la consulta sin ejecutarla aún

        return new ConsultCollection($query->paginate(config('izifact.items_per_page')));
    }

    private function getRecords($request){
        $value = $request->input('value');
        $value2 = $request->input('value2');
        $value3 = $request->input('value3');
        $value4 = $request->input('value4');
        $records = Consult::query(); // Crea una instancia de consulta
        if($value){
            $records = $records->whereRaw("DATE(created_at) = ?", [$value]);
        }
        if($value2 == "1" || $value2 == "0"){
            $records = $records->where("state", "{$value2}");
        }
        if($value3){
            $records = $records->where('name', "{$value3}");
        }
        if($value4){
            $records = $records->where('email', "{$value4}");
        }
        // $records = $query->latest();
        
        // Aplica filtros, ordenamientos, etc., según sea necesario
        // Ejemplo: $query->where('columna', '=', $valor);

        return $records;
    }

    public function consulttables(){
        $person = Consult::distinct()->pluck('name')->transform(function ($name) {
            return [
                'name' => $name,
            ];
        });
        $email = Consult::distinct()->pluck('email')->transform(function ($email) {
            return [
                'email' => $email,
            ];
        });
        return compact('person','email');
    }

    public function consultList(){
        return view("contactanos.list");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Consult  $consult
     * @return \Illuminate\Http\Response
     */
    public function show(Consult $consult)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Consult  $consult
     * @return \Illuminate\Http\Response
     */
    public function edit(Consult $consult)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Consult  $consult
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Consult $consult)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Consult  $consult
     * @return \Illuminate\Http\Response
     */
    public function destroy(Consult $consult)
    {
        //
    }
}
