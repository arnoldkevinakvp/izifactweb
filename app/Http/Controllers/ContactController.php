<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Area;
use App\Models\Consult;
use Illuminate\Http\Request;
use App\Http\Collection\ContactCollection;
use App\Http\Collection\ConsultCollection;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
     
    public function index()
    {
        $images=array(
            'venta' =>asset("/images/venta.png"),
            'compra' => asset("/images/compras.png"),
            'almacen' => asset("/images/almacen.png"),
            'reporte' => asset("/images/reporte.png"),
            'guias' => asset("/images/guias.png"),
            'documentos' => asset("/images/documentos.png"),
            'soluciones' => asset("/images/soluciones.png"),
            'asesoria' => asset("/images/asesoria.png"),
            'pagina_web' => asset("/images/pagina_web.png"),
            'movil_web' => asset("/images/movil_web.png"),
            'supermercado' => asset("/images/supermercado.png"),
            'farmacia' => asset("/images/farmacia.png"),
            'ferreteria' => asset("/images/ferreteria.png"),
            'restaurante' => asset("/images/restaurante.png"),
            'colegios' => asset("/images/colegios.png"),
            'hotel' => asset("/images/hotel.png"),
            'transporte' => asset("/images/transporte.png"),
            'libreria' => asset("/images/libreria.png"),
            'equipos' => asset("/images/equipos.png"),
            'gimnasios' => asset("/images/gimnasios.png"),
            'licoreria' => asset("/images/licoreria.png"),
            'panaderia' => asset("/images/panaderia.png"),
            'veterinaria' => asset("/images/veterinaria.png"),
            'vue_js' => asset("/images/vue_js.png"),
            'node_js' => asset("/images/node_js.png"),
            'maria_bd' => asset("/images/maria_bd.png"),
            'google' => asset("/images/google.png"),
            'digital_ocean' => asset("/images/digital_ocean.png"),
            'amazon' => asset("/images/amazon.png"),
            'jira' => asset("/images/jira.png"),
            'ibm' => asset("/images/ibm.png"),
            'docker' => asset("/images/docker.png"),
            'element' => asset("/images/element.png"),
            'herramientas' => asset("/images/herramientas-gestion.png"),
            'reportes' => asset("/images/reportes-automaticos.png"),
            'soporte' => asset("/images/soporte-personalizado.png"),
            'planes' => asset("/images/planes-flexibles.png"),
        );
        return view('inicio.inicio',compact('images'));
    }

    public function precios(){
        return view('prices.precios');
    }

    public function imagenesFlotante(){
        $pregunta = asset("/images/preguntas.svg");
        $whatsapp = asset("/images/icon-whatsapp2.svg");
        $servicio = asset("/images/servicio-al-cliente.png");
        $apoyo = asset("/images/apoyo-tecnico.png");
        return compact('pregunta','whatsapp','servicio','apoyo');
    }
    public function nosotros(){
        $images=array(
            'nosotros' => asset("/images/nosotros.jpg"),
            'mision' => asset("/images/mision.png"),
            'vision' => asset("/images/vision.png"),
            'demostracion' => asset("/images/demostracion.png"),
            'plan' => asset("/images/plan.png"),
            'implementacion' => asset("/images/implementacion.png"),
            'digitalizado' => asset("/images/digitalizado.png"),
            'emoji' => asset("/images/emoji.png"),
            'dispositivos' => asset("/images/dispositivos.png"),
            'software' => asset("/images/software.png"),
            'planes' => asset("/images/planes.png"),
            'clientes' => asset("/images/clientes.png"),
            'nube' => asset("/images/nube.png"),
            'soporte' => asset("/images/soporte.png"),
            'soluciones' => asset("/images/soluciones.png"),
            'url_pdf' => asset("/pdf/Brochure.pdf"),

        );
        return view('nosotros.index',compact('images'));
    }

    public function valores(){
        $images=array(
            'innovacion' => asset("/images/innovacion.png"),
            'simplicidad' => asset("/images/simplicidad.png"),
            'agilidad' => asset("/images/agilidad.png"),
            'confiabilidad' => asset("/images/confiabilidad.png"),
        );
        return view('valores.index',compact('images'));
    }

    public function indexContact(){
        return view("contactanos.form");
    }

    public function equipos(){
        $images=array(
            'P501A' =>asset("/images/P501A.png"),
            'P801A' =>asset("/images/P801A.png"),
            'A260M' => asset("/images/A260M.png"),
            'A260M_detalle' => asset("/images/A260M_detalle.png"),
            'A260M_detalle1' => asset("/images/A260M_detalle1.png"),
            'A260M_detalle2' => asset("/images/A260M_detalle2.png"),
            'XP_365' => asset("/images/XP_365.png"),
            'TG_4B_2054L' => asset("/images/TG_4B_2054L.png"),
            'YDA_9300D' => asset("/images/YDA_9300D.png"),
            'YHDAA' => asset("/images/YHDAA.png"),
            'RP320BT' => asset("/images/RP320BT.png"),
            'P501A_detalle' => asset("/images/P501A_detalle.png"),
            'P501A_detalle1' => asset("/images/P501A_detalle1.png"),
            'P501A_detalle2' => asset("/images/P501A_detalle2.png"),
            'pagina_web' => asset("/images/pagina_web.png"),
            'movil_web' => asset("/images/movil_web.png"),
            'supermercado' => asset("/images/supermercado.png"),
            'farmacia' => asset("/images/farmacia.png"),
            'ferreteria' => asset("/images/ferreteria.png"),
            'restaurante' => asset("/images/restaurante.png"),
            'colegios' => asset("/images/colegios.png"),
            'hotel' => asset("/images/hotel.png"),
            'transporte' => asset("/images/transporte.png"),
            'libreria' => asset("/images/libreria.png"),
            'equipos' => asset("/images/equipos.png"),
            'gimnasios' => asset("/images/gimnasios.png"),
            'licoreria' => asset("/images/licoreria.png"),
            'panaderia' => asset("/images/panaderia.png"),
            'veterinaria' => asset("/images/veterinaria.png"),
        );
        return view("equipos.list",compact('images'));
    }

    public function certificados(){
        $images=array(
            'certificado' => asset("/images/certificado.png")
        );
        return view("certificados.list",compact('images'));
    }

    public function facturacion(){
        $images=array(
            'proceso' => asset("/images/proceso.png")
        );
        return view("facturacion.list",compact('images'));
    }

    public function guias(){
        $images=array(
            'proceso_guia' => asset("/images/proceso_guia.png")
        );
        return view("guias.list",compact('images'));
    }

    public function userList(){
        return view("usuarios.list");
    }

    public function contactstate($id){
        $contact = Contact::find($id);
        if($contact->state == '0'){
            $contact->state = 1;
        }
        else{
            $contact->state = 0;
        }
        $contact->save();
        return [
            'success' => true,
            'message' => 'Estado cambiado con éxito'
        ];
    }

    public function personsRecords(Request $request){
        $query = $this->getRecords($request); // Obtén la consulta sin ejecutarla aún

        return new ContactCollection($query->paginate(config('izifact.items_per_page')));
    }

    private function getRecords($request){
        $value = $request->input('value');
        $value2 = $request->input('value2');
        $value3 = $request->input('value3');
        $value4 = $request->input('value4');
        $records = Contact::query(); // Crea una instancia de consulta
        if($value){
            $records = $records->whereRaw("DATE(created_at) = ?", [$value]);
        }
        if($value2 == "1" || $value2 == "0"){
            $records = $records->where("state", "{$value2}");
        }
        if($value3){
            $records = $records->where("plan", "{$value3}");
        }
        if($value4){
            $records = $records->where("mounth", "{$value4}");
        }
        // $records = $query->latest();
        
        // Aplica filtros, ordenamientos, etc., según sea necesario
        // Ejemplo: $query->where('columna', '=', $valor);

        return $records;
    }

    public function work(){
        $images=array(
            'factura' => asset("/images/factura.png"),
            'pos' => asset("/images/pos.png"),
            'web' => asset("/images/web.png"),
            'fuente' => asset("/images/fuente.png"),
            'asesoria' => asset("/images/asesoria.png"),
            'actualizaciones' => asset("/images/actualizaciones.png"),
            'seguridad' => asset("/images/seguridad.png"),
            'soluciones' => asset("/images/soluciones.png"),
            'desarrollador' => asset("/images/desarrollador.png"),
            'soporte' => asset("/images/soporte.png"),

        );
        return view("contactanos.work",compact('images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function images(){
        $venta = asset("/images/venta.png");
        $compra = asset("/images/compras.png");
        $almacen = asset("/images/almacen.png");
        $reporte = asset("/images/reporte.png");
        $guias = asset("/images/guias.png");
        $documentos = asset("/images/documentos.png");
        $soluciones = asset("/images/soluciones.png");
        $asesoria = asset("/images/asesoria.png");

        return compact('venta','compra','almacen','reporte','guias','documentos','soluciones','asesoria');
    }

    public function areas(){
        $areas = Area::all();

        return compact('areas');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function validateruc($ruc){
        $token = 'a7612a8c8c058e9093b2f49331ada5107f7e42a704db31ced38b5243f55fa1bf';

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://apiperu.dev/api/ruc/".$ruc."?api_token=".$token."",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_SSL_VERIFYPEER => false
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($response, true);
        if (!$data['success']) {
            return [
                'success' => false,
                'message' => 'El RUC ingresado es inválido'
            ];
        } else {
            return [
                'success' => true
            ];
        }
    }
    public function store(Request $request)
    {
            $contact = new Contact;
            $contact->fill($request->all());
            $contact->save();
            return [
                'success' => true,
                'message' => "Gracias por escribirnos, en breve un asesor te contactará."
            ];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        //
    }
}
