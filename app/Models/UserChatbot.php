<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserChatbot extends Model
{
    protected $table = 'user_chatbots';

    protected $fillable = ['name', 'phone','description'];

    public function questions(): HasMany
    {
        return $this->hasMany(Question::class);
    }
}
