<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    protected $fillable = ['question_id', 'response_text'];

    public function question(): BelongsTo
    {
        return $this->belongsTo(Question::class);
    }
}
