<?php

use Illuminate\Support\Facades\Route;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;
use Illuminate\Support\Facades\Auth;
Auth::routes();
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//------------------------------login---------------------------
Route::get('iniciar', 'App\Http\Controllers\AuthController@showLoginForm')->name('auth.login');
Route::get('register', 'App\Http\Controllers\AuthController@registrerLogin')->name('auth.register');

Route::post('login', 'App\Http\Controllers\AuthController@login');
Route::post('registrar', 'App\Http\Controllers\AuthController@registrar');
Route::post('logout', 'App\Http\Controllers\AuthController@logout');
Route::get('user', 'App\Http\Controllers\AuthController@user')->middleware('auth:sanctum');
https://izifact.com/auth/%7Bprovider%7D/callback
//----------------------------REGISTRO REDES---------------------
Route::get('auth/{provider}', 'App\Http\Controllers\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'App\Http\Controllers\AuthController@handleProviderCallback');
//--------------------------------------------------------------

Route::get('/', 'App\Http\Controllers\ContactController@index')->name('inicio.inicio');
Route::get('contact/images', 'App\Http\Controllers\ContactController@images');
Route::get('contact/imagenesFlotante', 'App\Http\Controllers\ContactController@imagenesFlotante');
Route::get('work/areas', 'App\Http\Controllers\ContactController@areas');
Route::post('contact', 'App\Http\Controllers\ContactController@store');
Route::post('consult', 'App\Http\Controllers\ConsultController@store');
Route::post('contact/consult', 'App\Http\Controllers\ConsultController@store');
Route::get('contact/validateruc/{ruc}', 'App\Http\Controllers\ContactController@validateruc');
Route::get('contact/index', 'App\Http\Controllers\ContactController@indexContact')->name('contactanos.form');
Route::get('precios', 'App\Http\Controllers\ContactController@precios')->name('prices.precios');
Route::get('nosotros', 'App\Http\Controllers\ContactController@nosotros')->name('nosotros.index');
Route::get('valores', 'App\Http\Controllers\ContactController@valores')->name('valores.index');
Route::get('nuestros-servicios', 'App\Http\Controllers\ContactController@work')->name('contactanos.work');
Route::get('usuarios-list', 'App\Http\Controllers\ContactController@userList')->name('usuarios.list');
Route::get('contacts/state/{id}', 'App\Http\Controllers\ContactController@contactstate');
Route::get('consults/state/{id}', 'App\Http\Controllers\ConsultController@consultstate');
Route::get('consult-list', 'App\Http\Controllers\ConsultController@consultList')->name('contactanos.list');
Route::get('persons/records', 'App\Http\Controllers\ContactController@personsRecords');
Route::get('consult/records', 'App\Http\Controllers\ConsultController@consultRecords');
Route::get('consult/tables', 'App\Http\Controllers\ConsultController@consulttables');

// Vistas para equipo
Route::get('equipos', 'App\Http\Controllers\ContactController@equipos')->name('equipos.list');

// Vistas para certificado
Route::get('certificados', 'App\Http\Controllers\ContactController@certificados')->name('certificados.list');

// Vistas para facturacion
Route::get('facturacion', 'App\Http\Controllers\ContactController@facturacion')->name('facturacion.list');

// Vistas para facturacion
Route::get('guias', 'App\Http\Controllers\ContactController@guias')->name('guias.list');

//ChatBot GPT
Route::post('chatbot/register', 'App\Http\Controllers\UserChatbotController@registerUser');
//Route::post('chatbot/message', 'App\Http\Controllers\UserChatbotController@processGPTMessage');
Route::post('chatbot/message', 'App\Http\Controllers\UserChatbotController@processGPTMessage');


Route::get('/sitemap.xml', function () {
    $sitemap = Sitemap::create();

    // Agrega las URLs de tu sitio al sitemap
    $sitemap->add(Url::create('/')->setPriority(1.0));
    $sitemap->add(Url::create('/contact')->setPriority(0.8));
    $sitemap->add(Url::create('/nosotros')->setPriority(0.8));
    $sitemap->add(Url::create('/valores')->setPriority(0.8));
    $sitemap->add(Url::create('/nuestros-servicios')->setPriority(0.8));
    $sitemap->add(Url::create('/equipos')->setPriority(0.8));
    $sitemap->add(Url::create('/facturacion')->setPriority(0.8));
    $sitemap->add(Url::create('/certificados')->setPriority(0.8));
    $sitemap->add(Url::create('/guias')->setPriority(0.8));
    $sitemap->add(Url::create('/precios')->setPriority(0.8));
    $sitemap->add(Url::create('/contact/index')->setPriority(0.8));

    return $sitemap->render('xml');
});


