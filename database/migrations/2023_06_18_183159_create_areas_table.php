<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('state')->default(true);
            $table->timestamps();
        });

        DB::table('areas')->insert([
            ['name' => 'Programación'],
            ['name' => 'Full Stack'],
            ['name' => 'Diseño Web'],
            ['name' => 'Ventas'],
            ['name' => 'Soporte']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas');
    }
}
