/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Vue from 'vue'
import Axios from 'axios'
import ElementUI from 'element-ui'
import lang from 'element-ui/lib/locale/lang/es'
import locale from 'element-ui/lib/locale'
import 'element-ui/lib/theme-chalk/index.css';
import VueSweetalert2 from 'vue-sweetalert2';

import 'sweetalert2/dist/sweetalert2.min.css';
window.Vue = require('vue').default;
locale.use(lang)

ElementUI.Select.computed.readonly = function () {
    const isIE = !this.$isServer && !Number.isNaN(Number(document.documentMode));
    return !(this.filterable || this.multiple || !isIE) && !this.visible;
};
export default ElementUI;
Vue.use(ElementUI, { size: 'small' })
Vue.use(VueSweetalert2);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
Vue.prototype.$eventHub = new Vue()
Vue.prototype.$http = Axios

Vue.component('inicio-web', require('./inicio/inicio.vue').default);
Vue.component('prices-izi', require('./prices/index.vue').default);
Vue.component('contact-izi', require('./contact/index.vue').default);
Vue.component('form-izi', require('./contact/form.vue').default);
Vue.component('work-izi', require('./contact/work.vue').default);
Vue.component('form-izi-inicio', require('./contact/form_inicio.vue').default);
Vue.component('precios-index', require('./prices/precios.vue').default);
Vue.component('nosotros-index', require('./nosotros/index.vue').default);
Vue.component('valores-index', require('./valores/index.vue').default);
Vue.component('user-list', require('./usuarios/list.vue').default);
Vue.component('consult-list', require('./contact/list.vue').default);
Vue.component('equipos-list', require('./equipos/list.vue').default);
Vue.component('certificados-list', require('./certificados/list.vue').default);
Vue.component('facturacion-list', require('./facturacion/list.vue').default);
Vue.component('guias-list', require('./guias/list.vue').default);
Vue.component('conocenos', require('./conocenos/conocenos.vue').default);
Vue.component('tipo-empresas', require('./conocenos/tipos.vue').default);
Vue.component('equipos-detalle', require('./equipos/equipos_detalle.vue').default);
Vue.component('imagen-flotante', require('./flotante/imagen.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#main',
    data: {
        user: null,
    },
    created() {
        this.fetchUser();

        // Escucha eventos de inicio o cierre de sesión
        this.$eventHub.$on('user-logged-in', (user) => {
            this.user = user;
        });
        this.$eventHub.$on('user-logged-out', () => {
            this.user = null;
        });
    },
    methods: {
        async fetchUser() {
            try {
                const response = await this.$http.get('/user');
                this.user = response.data;
            } catch {
                this.user = null; // Si no hay usuario autenticado
            }
        },
    },
});
