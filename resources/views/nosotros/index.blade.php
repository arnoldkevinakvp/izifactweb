@extends('layouts.app')

@section('content')

<nosotros-index :images ="{{ json_encode($images) }}"></nosotros-index>

@endsection