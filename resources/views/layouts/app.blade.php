<!DOCTYPE html>
<html
    lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>IziFact</title>
    <link rel="shortcut icon" href="/images/izifact.ico" type="image/x-icon">
    
    <link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"/>

    <link async href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link async href="{{ asset('css/owl.carousel.css') }}" rel="stylesheet">
    <link async href="{{ asset('css/formulario.css') }}" rel="stylesheet">
    <link async href="{{ asset('css/btn-whatsapp.css') }}" rel="stylesheet">

    <script defer src="{{ mix('js/app.js') }}"></script>
    <script src="{{ asset('js/carousel.js') }}" defer></script>
    <script src="{{ asset('js/clientes.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-touch/2.0.0/vue-touch.min.js"></script>
</head>
<body>
    
    @include('layouts.partials.header')
        <div id="main">
            @yield('content')
        </div>
    @include('layouts.partials.footer')
    <div class="social-buttons">
        <a href="https://www.facebook.com/Izifact.ti" target="_blank"><i class="fa-brands fa-facebook"></i></a>
        <a href="https://www.instagram.com/izifact.erp/" target="_blank"><i class="fa-brands fa-instagram"></i></a>
        <a href="https://www.tiktok.com/@izifact.erp" target="_blank"><i class="fa-brands fa-tiktok"></i></a>
        <a href="https://www.youtube.com/@izifacterp5272" target="_blank"><i class="fa-brands fa-youtube"></i></a>
        <a href="https://www.linkedin.com/company/izifact-erp/" target="_blank"><i class="fa-brands fa-linkedin"></i></a>
    </div>
    <script src="{{ asset('js/main.js') }}"></script>
</body>
</html>
<link async href="{{ asset('css/elements.css') }}" rel="stylesheet">