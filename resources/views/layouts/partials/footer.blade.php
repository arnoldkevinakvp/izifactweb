<footer>
  <div class="footer-section" style="text-align: center;">
    <img src="{{ asset('images/logo2.png') }}" alt="Logo de la empresa" style="margin-right: 10px;">
    <p style="margin: 0;">Un sistema eficiente para gestionar tus ventas, compras e inventarios, utilizado por múltiples empresas en todo el país.</p>
    <a href="https://www.facebook.com/Izifact.ti" target="_blank"><i class="fa-brands fa-facebook"></i></a>
    <a href="https://www.instagram.com/izifact/" target="_blank"><i class="fa-brands fa-instagram"></i></a>
    <a href="https://wa.link/gdldfo" target="_blank"><i class="fa-brands fa-whatsapp"></i></a>
    <a href="https://www.linkedin.com/company/izifact-erp/" target="_blank"><i class="fa-brands fa-linkedin"></i></a>
  </div>
  <div class="footer-section">
    <h3>EMPRESA</h3>
    <ul>
        <li>
          <a href="{{route('nosotros.index')}}">¿Quienes Somos?</a>
        </li>
        <li>
          <a href="{{route('contactanos.work')}}">Nuestros Servicios</a>
        </li>
        <li>
          <a href="#">Preguntas Frecuentes</a>
        </li>
    </ul>
  </div>
  <div class="footer-section">
    <h3>SOLUCIONES</h3>
    <ul>
        <li>
            <a href="{{route('facturacion.list')}}">Factura Electrónica</a>
        </li>
        <li>
            <a href="{{route('contactanos.work')}}">Desarrollos a medida</a>
        </li>
        <li>
            <a href="{{route('contactanos.work')}}">Sistema ERP</a>
        </li>
    </ul>
  </div>
  <div class="footer-section">
      <h3>CONTACTO</h3>
      <div class="contact-info">
        <i class="fas fa-envelope"></i>
        <div class="contact-text">
          <p>ventas@izifact.com</p>
          <p>soporte@izifact.com</p>
        </div>
      </div>
      <div class="contact-info">
        <i class="fa-sharp fa-solid fa-phone"></i>
          <div class="contact-text">
            <p>943217021</p>
          </div>
      </div>
      <div class="contact-info">
        <i class="fa-brands fa-whatsapp"></i>
        <div class="contact-text">
          <p>943217021</p>
          <p>932823943</p>
        </div>
      </div>
    </div>
  </div>
</footer>

<div class="bottom-footer">
  <div class="left">
    <p>
    </p>
  </div>
  <div class="right">
    <a href="#">Términos y condiciones</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="#">Políticas y privacidad</a>
  </div>
</div>