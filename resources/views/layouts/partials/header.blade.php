<header class="header">
    <nav class="nav container">
        <div class="nav__data">
            <a href="{{route('inicio.inicio')}}" class="nav__logo">
                <img src="{{ asset('images/LOGO.png') }}"
                alt="Logo">
                <img src="{{ asset('images/logo5.png') }}"
                alt="Logo">
            </a>
            <div class="nav__toggle" id="nav-toggle">
                <i class="fa-solid fa-bars nav__toggle-menu"></i>
                <i class="fa-solid fa-xmark nav__toggle-close"></i>
            </div>
        </div>

        <!--=============== NAV MENU ===============-->
        <div class="nav__menu" id="nav-menu">
            <ul class="nav__list">
                <li>
                    <a href="{{route('inicio.inicio')}}" class="nav__link">INICIO</a>
                </li>

                <!--=============== DROPDOWN 1 ===============-->
                <li class="dropdown__item">
                    <div class="nav__link dropdown__button">
                        EMPRESA <i class="fa-solid fa-angle-down dropdown__arrow"></i>
                    </div>

                    <div class="dropdown__container">
                        <div class="dropdown__content">
                            <div class="dropdown__group">
                                <div class="dropdown__icon">
                                    <i class="fa-solid fa-people-roof"></i>
                                </div>

                                <span class="dropdown__title">Nosotros</span>

                                <ul class="dropdown__list">
                                    <li>
                                        <a href="{{route('nosotros.index')}}" class="dropdown__link">Transformando negocios a través <br> de la digitalización</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="dropdown__group">
                                <div class="dropdown__icon">
                                    <i class="fa-solid fa-heart-circle-plus"></i>
                                </div>

                                <span class="dropdown__title">Valores</span>

                                <ul class="dropdown__list">
                                    <li>
                                        <a href="{{route('valores.index')}}" class="dropdown__link">Un camino en la innovación <br>digital de tu negocio</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="dropdown__group">
                                <div class="dropdown__icon">
                                    <i class="fa-solid fa-store"></i>
                                </div>

                                <span class="dropdown__title">Nuestros servicios</span>

                                <ul class="dropdown__list">
                                    <li>
                                        <a href="{{route('contactanos.work')}}" class="dropdown__link">Conoce nuestros servicios  <br> disponibles</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>

                <!--=============== DROPDOWN 2 ===============-->

                <li class="dropdown__item">                      
                    <div class="nav__link dropdown__button">
                        RECURSOS <i class="fa-solid fa-angle-down dropdown__arrow"></i>
                    </div>

                    <div class="dropdown__container">
                        <div class="dropdown__content">
                            <div class="dropdown__group">
                                <div class="dropdown__icon">
                                    <i class="fa-solid fa-print"></i>
                                </div>

                                <span class="dropdown__title">Equipos</span>

                                <ul class="dropdown__list">
                                    <li>
                                        <a href="{{route('equipos.list')}}" class="dropdown__link">Conoce nuestros equipos</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="dropdown__group">
                                <div class="dropdown__icon">
                                    <i class="fa-solid fa-file-invoice-dollar"></i>
                                </div>

                                <span class="dropdown__title">Facturación</span>

                                <ul class="dropdown__list">
                                    <li>
                                        <a href="{{route('facturacion.list')}}" class="dropdown__link">Facturación electrónica</a>
                                    </li>
                                    <li>
                                        <a href="{{route('guias.list')}}" class="dropdown__link">Guías de remisión electrónica</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="dropdown__group">
                                <div class="dropdown__icon">
                                    <i class="fa-regular fa-folder"></i>
                                </div>

                                <span class="dropdown__title">Certificado digital</span>

                                <ul class="dropdown__list">
                                    <li>
                                        <a href="{{route('certificados.list')}}" class="dropdown__link">Certificado digital para <br>facturación electrónica</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>

                <li>
                    <a href="{{route('prices.precios')}}" class="nav__link">PRECIOS</a>
                </li>

                <li>
                    <a href="{{route('contactanos.form')}}" class="nav__link">CONTÁCTANOS</a>
                </li>

                <!--=============== DROPDOWN 3 ===============-->
                <!-- <li class="dropdown__item">
                    <div class="nav__link dropdown__button">
                        CONTÁCTANOS 
                    </div>

                    <div class="dropdown__container">
                        <div class="dropdown__content">
                            <div class="dropdown__group">
                                <div class="dropdown__icon">
                                    <i class="ri-community-line"></i>
                                </div>

                                <span class="dropdown__title">About us</span>

                                <ul class="dropdown__list">
                                    <li>
                                        <a href="#" class="dropdown__link">About us</a>
                                    </li>
                                    <li>
                                        <a href="#" class="dropdown__link">Support</a>
                                    </li>
                                    <li>
                                        <a href="#" class="dropdown__link">Contact us</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="dropdown__group">
                                <div class="dropdown__icon">
                                    <i class="ri-shield-line"></i>
                                </div>

                                <span class="dropdown__title">Safety and quality</span>

                                <ul class="dropdown__list">
                                    <li>
                                        <a href="#" class="dropdown__link">Cookie settings</a>
                                    </li>
                                    <li>
                                        <a href="#" class="dropdown__link">Privacy Policy</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li> -->
                <!-- Botones de Login / Logout -->
                <li>
                    @auth
                        <a href="{{ route('logout') }}" 
                        class="nav__link"
                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            CERRAR SESIÓN
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @else
                        <a href="{{ route('auth.login') }}" class="nav__link">INICIAR SESIÓN</a>
                    @endauth
                </li>

            </ul>
        </div>
    </nav>
</header>