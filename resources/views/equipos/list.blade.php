@extends('layouts.app')

@section('content')

<equipos-list :images ="{{ json_encode($images) }}"></equipos-list>

@endsection