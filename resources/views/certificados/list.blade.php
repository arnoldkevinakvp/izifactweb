@extends('layouts.app')

@section('content')

<certificados-list :images ="{{ json_encode($images) }}"></certificados-list>

@endsection