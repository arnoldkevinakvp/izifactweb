@extends('layouts.app')

@section('content')

<facturacion-list :images ="{{ json_encode($images) }}"></facturacion-list>

@endsection