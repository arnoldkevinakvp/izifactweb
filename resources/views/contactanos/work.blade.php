@extends('layouts.app')

@section('content')

<work-izi :images ="{{ json_encode($images) }}"></work-izi>

@endsection