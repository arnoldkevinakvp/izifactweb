@extends('layouts.app')

@section('content')

<valores-index :images ="{{ json_encode($images) }}"></valores-index>

@endsection