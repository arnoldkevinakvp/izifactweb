<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Registrate</title>
    <link rel="stylesheet" href="https://unpkg.com/element-plus/dist/index.css">
    <link async href="{{ asset('css/login.css') }}" rel="stylesheet">
</head>
<body>
    <div class="register-container" id="app">
        <div class="register-header">
            <h1>Crea tu cuenta</h1>
            <p>Completa el formulario para registrarte</p>
        </div>
        <form>
            <div class="input-group">
                <label for="fullname">Nombre completo</label>
                <input type="text" id="fullname" name="fullname" required placeholder="Tu nombre completo" v-model="form.name">
            </div>
            <div class="input-group">
                <label for="email">Correo Electrónico</label>
                <input type="email" id="email" name="email" required placeholder="tu@email.com" v-model="form.email">
            </div>
            <div class="input-group">
                <label for="password">Contraseña</label>
                <input type="password" id="password" name="password" required placeholder="Crea una contraseña segura" v-model="form.password">
            </div>
            <div class="input-group">
                <label for="confirm-password">Confirmar Contraseña</label>
                <input type="password" id="confirm-password" name="confirm-password" required placeholder="Repite tu contraseña" v-model="password_confirm">
            </div>
            <div class="terms">
                <input type="checkbox" id="terms" name="terms" required>
                <label for="terms">Acepto los <a href="#">Términos y Condiciones</a> y la <a href="#">Política de Privacidad</a></label>
            </div>
            <button type="submit" class="btn">Crear Cuenta</button>
        </form>
        <div class="register-footer">
            <p>¿Ya tienes una cuenta? <a href="/iniciar">Inicia sesión</a></p>
        </div>
        <div class="social-register">
            <p>O regístrate con:</p>
        </div>
        <div class="social-register">
            <p>O regístrate con:</p>
            <a href="/auth/google" class="social-btn">
                <img src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg" alt="Google">
            </a>
            <a href="/auth/facebook" class="social-btn">
                <img src="https://upload.wikimedia.org/wikipedia/commons/b/b8/2021_Facebook_icon.svg" alt="Facebook">
            </a>
        </div>
    </div>
    <script src="https://unpkg.com/vue@3"></script>
    <script src="https://unpkg.com/element-plus"></script>
    <script>
        const { createApp } = Vue;

        createApp({
            data() {
                return {
                    form: {},
                    password_confirm: ''
                };
            },
            methods: {
                async create() {
                    await this.initform();
                },
                initform(){
                    this.form = {
                        email: '',
                        password: '',
                        name : '',
                        ruc : '',
                        celular: ''
                    },
                },
                handleLogin() {
                    this.$refs.loginForm.validate((valid) => {
                        if (valid) {
                            document.querySelector('form').submit(); // Submit form
                        } else {
                            console.log('Error in form submission');
                            return false;
                        }
                    });
                },
            },
        }).mount('#app');
    </script>
</body>
</html>