<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Login</title>
    <link rel="stylesheet" href="https://unpkg.com/element-plus/dist/index.css">
    <link async href="{{ asset('css/login.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app" class="login-container">
        <div class="login-header">
            <h1>Bienvenido de nuevo</h1>
            <p>Por favor, ingresa tus credenciales para acceder</p>
        </div>
            <div class="input-group">
                <label for="email">Correo Electrónico</label>
                <input v-model="loginForm.email" type="email" id="email" name="email" required placeholder="tu@email.com" autocomplete ="false">
            </div>
            <div class="input-group">
                <label for="password">Contraseña</label>
                <input v-model="loginForm.password" type="password" id="password" name="password" required placeholder="Tu contraseña" autocomplete="off">
            </div>
            <div class="remember-me">
                <input type="checkbox" id="remember" name="remember" v-model="loginForm.remember">
                <label for="remember">Recordarme</label>
            </div>
            <button type="submit" class="btn" @click="handleLogin">Iniciar Sesión</button>
        <div class="login-footer">
            <p><a href="#">¿Olvidaste tu contraseña?</a></p>
            <p>¿No tienes una cuenta? <a :href="`/register`" target="_blank">Regístrate</a></p>
        </div>3.
        <div class="social-login">
            <a href="#" class="social-btn">
                <img src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg" alt="Google">
            </a>
            <a href="#" class="social-btn">
                <img src="https://upload.wikimedia.org/wikipedia/commons/b/b8/2021_Facebook_icon.svg" alt="Facebook">
            </a>
            <a href="#" class="social-btn">
                <img src="https://upload.wikimedia.org/wikipedia/commons/4/4f/Twitter-logo.svg" alt="Twitter">
            </a>
        </div>
    </div>
    <script src="https://unpkg.com/vue@3"></script>
    <script src="https://unpkg.com/element-plus"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        const { createApp } = Vue;
        createApp({
            data() {
                return {
                    loginForm: {},
                    resource: "login"
                };
            },
            methods: {
                async create() {
                    await this.initform();
                },
                initform(){
                    this.loginForm = {
                        email: '',
                        password: '',
                        remember: false,
                    },
                },
                async handleLogin() {
                    try {
                        const response = await fetch(`${this.resource}`, {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                                'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
                            },
                            body: JSON.stringify(this.loginForm),
                        });

                        if (!response.ok) {
                            throw new Error(`HTTP error! status: ${response.status}`);
                        }

                        const data = await response.json();

                        if (data.success) {
                            alert('Inicio de sesión exitoso');
                            console.log(data.message);
                        } else {
                            alert('Error al iniciar sesión');
                            console.log(data.message);
                        }
                    } catch (error) {
                        console.error('Error:', error);
                        alert('Hubo un problema al iniciar sesión.');
                    }
                },
            },
        }).use(ElementPlus).mount('#app');
    </script>
</body>
</html>
