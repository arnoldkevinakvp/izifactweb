@extends('layouts.app')

@section('content')

<guias-list :images ="{{ json_encode($images) }}"></guias-list>

@endsection